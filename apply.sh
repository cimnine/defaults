#!/bin/bash

# heavily inspired by https://mths.be/macos

function finder {
  # Show hidden files by default
  defaults write com.apple.finder AppleShowAllFiles -bool true

  # Show all file extensions
  defaults write NSGlobalDomain AppleShowAllExtensions -bool true

  # Enable Text Selection in Quick Look Windows
  defaults write com.apple.finder QLEnableTextSelection -bool true

  # Enable the status bar in Finder
  defaults write com.apple.finder ShowStatusBar -bool true

  # Show Path Bar in Finder
  defaults write com.apple.finder ShowPathbar -bool true

  # Set Current Folder as Default Search Scope
  defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

  # Show External Hard Drives on Desktop
  defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true

  # Show Internal Hard Drives on Desktp
  defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true

  # Show Network Drives on Desktop
  defaults write com.apple.finder ShowMountedServersOnDesktop -bool true

  # Show Removable Media on Desktop
  defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool true

  # Disable Animations
  defaults write com.apple.finder DisableAllAnimations -bool true

  # Open New Window when Removable Disk is Connected
  defaults write com.apple.finder OpenWindowForNewRemovableDisk -bool true

  # Disable Warning when chaning file endings
  defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

  # Don't write .DS_Store to network shares and USB devices
  defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
  defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

  # Allow quitting via CMD+Q (also hides Desktop Icons)
  defaults write com.apple.finder QuitMenuItem -bool true

  # Disable Finder animations
  defaults write com.apple.finder DisableAllAnimations -bool true

  # Home as default location for Finder windows
  defaults write com.apple.finder NewWindowTarget -string "PfHm"
  defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/"

  # Use small sidebar icons
  defaults write NSGlobalDomain "NSTableViewDefaultSizeMode" -int "1"

  # Use list view in all Finder windows by default
  defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"

  # Always show title bar icon
  defaults write com.apple.universalaccess "showWindowTitlebarIcons" -bool "true"
  defaults write NSGlobalDomain "NSToolbarTitleViewRolloverDelay" -float "0"

  # Enable snap-to-grid for icons on the desktop and in other icon views
  /usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
  /usr/libexec/PlistBuddy -c "Set :FK_StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
  /usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist

  # Show the Library folder
  chflags nohidden ~/Library && xattr -d com.apple.FinderInfo ~/Library

  # Expand the following File Info panes:
  # “General”, “Open with”, and “Sharing & Permissions”
  defaults write com.apple.finder FXInfoPanesExpanded -dict \
    General -bool true \
    OpenWith -bool true \
    Privileges -bool true

  killall Finder
}

function dialogs {
  # Always show expanded save dialogs
  defaults write -g NSNavPanelExpandedStateForSaveMode -bool true
  defaults write -g NSNavPanelExpandedStateForSaveMode2 -bool true

  # Don't Save to iCloud
  defaults write -g NSDocumentSaveNewDocumentsToCloud -bool false

  # Always show expanded print dialogs
  defaults write -g PMPrintingExpandedStateForPrint -bool true
  defaults write -g PMPrintingExpandedStateForPrint2 -bool true

  # Quit Printer Applications when printing is done
  defaults write com.apple.print.PrintingPrefs "Quit When Finished" -bool true
}

function dock {
  # Make Hidden App Icons Translucent in the Dock
  defaults write com.apple.dock showhidden -bool YES

  # Minimize to Application
  defaults write com.apple.dock minimize-to-application -bool true

  # Auto-Hide Dock
  defaults write com.apple.dock autohide -bool true

  # Disable Auto-Hide Delay
  defaults write com.apple.dock autohide-delay -float 0
  defaults write com.apple.dock autohide-time-modifier -float 0

  # Don't Show Recent Applications
  defaults write com.apple.dock show-recents -bool false

  # Disable Launch Animation
  defaults write com.apple.dock launchanim -bool false

  # Show Indications for Running Apps
  defaults write com.apple.dock show-process-indicators -bool true

  # Disable Launchpad Gesture
  defaults write com.apple.dock showLaunchpadGestureEnabled -int 0

  # Preffered Tile Size of Dock
  defaults write com.apple.dock "tilesize" -int "50"

  # Changes the minimization effect to a less annoying one
  defaults write com.apple.dock "mineffect" -string "scale"

  killall Dock
}

function timemachine {
  # Do not offer new disks for backup
  defaults write com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true
}

function mac {
  # Disable automatic termination of inactive apps
  defaults write -g NSDisableAutomaticTermination -bool true

  # Set Highlights Color to Graphite
  defaults write -g AppleHighlightColor -string "0.847059 0.847059 0.862745 Graphite"

  # Set Help Window to Non-Floating Mode
  defaults write com.apple.helpviewer DevMode -bool true

  # Reveal IP address, hostname, etc. when clicking the clok in login window
  sudo defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName

  # Disable automatic capitalization
  defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false

  # Disable smart dashes
  defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

  # Disable period substition on double space
  defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false

  # Disable smart quotes
  defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

  # Disable auto-correct
  defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

  # Use scroll gesture with the Ctrl (^) modifier key to zoom
  defaults write com.apple.universalaccess closeViewScrollWheelToggle -bool true
  defaults write com.apple.universalaccess HIDScrollZoomModifierMask -int 262144

  # Set Localization
  defaults write NSGlobalDomain AppleLanguages -array "en-CH" "en-US" "de-CH"
  defaults write NSGlobalDomain AppleLocale -string "en_CH"
  defaults write NSGlobalDomain AppleMeasurementUnits -string "Centimeters"
  defaults write NSGlobalDomain AppleMetricUnits -bool true

  # Show language menu on login screen
  sudo defaults write /Library/Preferences/com.apple.loginwindow showInputMenu -bool true

  # Save Screenshots to Desktop
  defaults write com.apple.screencapture location -string "~/Desktop"

  # Use plain text mode for new TextEdit documents
  defaults write com.apple.TextEdit RichText -bool false

  # Open and save files as UTF-8 in TextEdit
  defaults write com.apple.TextEdit PlainTextEncoding -int 4
  defaults write com.apple.TextEdit PlainTextEncodingForWrite -int 4

  # Enable the debug menu in Disk Utility
  defaults write com.apple.DiskUtility DUDebugMenuEnabled -bool true
  defaults write com.apple.DiskUtility advanced-image-options -bool true

  # Enable the automatic update check & download in App Store
  defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true
  defaults write com.apple.SoftwareUpdate AutomaticDownload -int 1
  defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 1
  defaults write com.apple.commerce AutoUpdate -bool true
  defaults write com.apple.commerce AutoUpdateRestartRequired -bool false

  # Check for software updates daily, not just once per week
  defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

  # Do not automatically download apps purchased on other macs
  defaults write com.apple.SoftwareUpdate ConfigDataInstall -int 0

  # Do not open Photos automatically
  defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true

  # Scrollbar: Jump to the spot that's clicked
  defaults write -globalDomain AppleScrollerPagingBehavior -bool true

  # Disable CMD+M (Minimize Window)
  defaults write -g NSUserKeyEquivalents -dict-add 'Minimize' '\0'
}

function spaces {
  # Disable Dashboard
  defaults write com.apple.dashboard mcx-disabled -bool true
  defaults write com.apple.dock dashboard-in-overlay -bool true

  # Don't automatically rearrange spaces
  defaults write com.apple.dock mru-spaces -bool false

  # Disable click on wallpaper to show desktop
  defaults write com.apple.WindowManager EnableStandardClickToShowDesktop 0

  # Don't switch to open windows of an application on CMD+Tab
  defaults write -g AppleSpacesSwitchOnActivate -bool false
}

function sound {
  # Disable Sounds
  defaults write NSGlobalDomain com.apple.sound.beep.feedback -int 0
  defaults write NSGlobalDomain com.apple.sound.uiaudio.enabled -int 0
  defaults write NSGlobalDomain com.apple.sound.beep.volume -float 0.0

  sudo nvram SystemAudioVolume=" "
}

function input {
  # Trackpad: enable tap to click for this user and for the login screen
  defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
  defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
  defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

  # Enable full keyboard access for all controls
  # (e.g. enable Tab in modal dialogs)
  defaults write NSGlobalDomain AppleKeyboardUIMode -int 3

  # Disable press-and-hold for keys in favor of key repeat
  defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

  # Set a blazingly fast keyboard repeat rate
  defaults write NSGlobalDomain KeyRepeat -int 1
  defaults write NSGlobalDomain InitialKeyRepeat -int 10

  # Hot corners
  # Possible values:
  #  0: no-op
  #  2: Mission Control
  #  3: Show application windows
  #  4: Desktop
  #  5: Start screen saver
  #  6: Disable screen saver
  #  7: Dashboard
  # 10: Put display to sleep
  # 11: Launchpad
  # 12: Notification Center
  # 13: Lock Screen
  # Top left screen corner → Desktop
  defaults write com.apple.dock wvous-tl-corner -int 4
  defaults write com.apple.dock wvous-tl-modifier -int 0
  # Top right screen corner → Show application windows
  defaults write com.apple.dock wvous-tr-corner -int 3
  defaults write com.apple.dock wvous-tr-modifier -int 0
  # Bottom left screen corner → Desktop
  defaults write com.apple.dock wvous-bl-corner -int 4
  defaults write com.apple.dock wvous-bl-modifier -int 0
  # Bottom right screen corner → Mission Control
  defaults write com.apple.dock wvous-br-corner -int 2
  defaults write com.apple.dock wvous-br-modifier -int 0
}

function safari {
  # Show the full URL in the address bar (note: this still hides the scheme)
  defaults write com.apple.Safari ShowFullURLInSmartSearchField -bool true

  # Press Tab to highlight each item on a web page
  defaults write com.apple.Safari WebKitTabToLinksPreferenceKey -bool true
  defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2TabsToLinks -bool true

  # Set Safari’s home page to `about:blank`
  defaults write com.apple.Safari HomePage -string "about:blank"

  # Prevent Safari from opening ‘safe’ files automatically after downloading
  defaults write com.apple.Safari AutoOpenSafeDownloads -bool false

  # Hide Safari’s bookmarks bar
  defaults write com.apple.Safari ShowFavoritesBar -bool false

  # Hide Safari’s sidebar in Top Sites
  defaults write com.apple.Safari ShowSidebarInTopSites -bool false

  # Disable Safari’s thumbnail cache for History and Top Sites
  defaults write com.apple.Safari DebugSnapshotsUpdatePolicy -int 2

  # Enable Safari’s debug menu
  defaults write com.apple.Safari IncludeInternalDebugMenu -bool true

  # Make Safari’s search banners default to Contains instead of Starts With
  defaults write com.apple.Safari FindOnPageMatchesWordStartsOnly -bool false

  # Remove useless icons from Safari’s bookmarks bar
  defaults write com.apple.Safari ProxiesInBookmarksBar "()"

  # Enable the Develop menu and the Web Inspector in Safari
  defaults write com.apple.Safari IncludeDevelopMenu -bool true
  defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true
  defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled -bool true

  # Add a context menu item for showing the Web Inspector in web views
  defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

  # Enable continuous spellchecking
  defaults write com.apple.Safari WebContinuousSpellCheckingEnabled -bool true
  # Disable auto-correct
  defaults write com.apple.Safari WebAutomaticSpellingCorrectionEnabled -bool false

  # Disable AutoFill
  defaults write com.apple.Safari AutoFillFromAddressBook -bool false
  defaults write com.apple.Safari AutoFillPasswords -bool false
  defaults write com.apple.Safari AutoFillCreditCardData -bool false
  defaults write com.apple.Safari AutoFillMiscellaneousForms -bool false

  # Warn about fraudulent websites
  defaults write com.apple.Safari WarnAboutFraudulentWebsites -bool true

  # Enable “Do Not Track”
  defaults write com.apple.Safari SendDoNotTrackHTTPHeader -bool true

  # Update extensions automatically
  defaults write com.apple.Safari InstallExtensionUpdatesAutomatically -bool true
}

function terminal {
  # Use UTF-8 in Terminal
  defaults write com.apple.terminal StringEncodings -array 4

  # Don’t display the annoying prompt when quitting iTerm
  defaults write com.googlecode.iterm2 PromptOnQuit -bool false
}

function activitymon {
  # Show the main window when launching Activity Monitor
  defaults write com.apple.ActivityMonitor OpenMainWindow -bool true

  # Visualize CPU usage in the Activity Monitor Dock icon
  defaults write com.apple.ActivityMonitor IconType -int 5

  # Show all processes in Activity Monitor
  defaults write com.apple.ActivityMonitor ShowCategory -int 0

  # Sort Activity Monitor results by CPU usage
  defaults write com.apple.ActivityMonitor SortColumn -string "CPUUsage"
  defaults write com.apple.ActivityMonitor SortDirection -int 0

  # Set update frequency to very often
  defaults write com.apple.ActivityMonitor "UpdatePeriod" -int "1"

  killall Activity\ Monitor
}

finder
dialogs
dock
timemachine
mac
spaces
input
terminal
activitymon

echo "Done. Better reboot now."
