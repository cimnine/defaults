# defaults

This is my collection of macOS defaults that I apply to new installations of macOS.

## Usage

```sh
./apply.sh
```

## References

* https://mths.be/macos (https://github.com/mathiasbynens/dotfiles/blob/main/.macos)
* https://macos-defaults.com/

